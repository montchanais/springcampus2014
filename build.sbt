// build.sbt file for the CERN Spring Campus 2014 Scala & Play Hands-On Presentation
// Copyright (c) 2014 Jan Janke (CERN)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

organization := "org.bitbucket.montchanais"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.4"

scalacOptions ++=
  Seq(
    "-feature"
  )

lazy val root = project.in( file( "." ) ).aggregate( core, ui )

lazy val core = project.in( file( "core" ) )

lazy val ui = project.in( file( "ui" ) ).dependsOn( core % "compile->compile;test->test" )

