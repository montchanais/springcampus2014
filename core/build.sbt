scalacOptions ++=
  Seq(
    "-feature"
  )

libraryDependencies <+= (scalaVersion)("org.scala-lang" % "scala-reflect" % _)

libraryDependencies ++=
  Seq(
    "org.mongodb" % "mongo-java-driver" % "2.11.4",
    "joda-time" % "joda-time" % "2.3",
    "org.joda" % "joda-convert" % "1.6",
    "org.scalatest" %% "scalatest" % "2.1.0" % "test",
    "com.typesafe" %% "scalalogging-log4j" % "1.1.0",
    "org.apache.logging.log4j" % "log4j-core" % "2.0-rc1"
  )
