/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.context

trait AppInfo {
  val appName: String
  val appShortName: String
  val appVersion: String
  val appProjectLink: String
  val appCopyrightYears: String
  val appCopyrightOrg: String
  val appCopyright: String
}
