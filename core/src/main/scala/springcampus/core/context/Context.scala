/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.context

import springcampus.core.db.{MDomainCompanionRegistry, PersistenceProvider}
import springcampus.core.service.InitializationService

trait Context {

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Essential base service (app info, persistence etc.)                                               *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  val appInfo: AppInfo
  val domainCompanionRegistry: MDomainCompanionRegistry
  val persistenceProvider: PersistenceProvider

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * General services                                                                                  *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  val initializationService: InitializationService
}
