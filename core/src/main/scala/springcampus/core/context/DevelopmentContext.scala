/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.context

import springcampus.core.db.{MongoPersistenceProvider, MDomainCompanion, MDomainCompanionRegistry}
import com.mongodb.ServerAddress
import springcampus.core.domain.{Country, Club}
import springcampus.core.service.InitializationService

class DevelopmentContext protected[context]() extends Context {

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Essential base service (app info, persistence etc.)                                               *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  lazy val appInfo = SpringCampusAppInfo
  lazy val domainCompanionRegistry = new MDomainCompanionRegistry {
    protected val domainCompanionsByName: Map[String, MDomainCompanion[_]] =
      Map(
        classOf[Club].getSimpleName -> Club,
        classOf[Country].getSimpleName -> Country
      )
  }

  lazy val persistenceProvider = new MongoPersistenceProvider( domainCompanionRegistry, List( new ServerAddress("localhost", 27017) ), "springcampus" )

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * General services                                                                                  *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  lazy val initializationService = new InitializationService {
    val persistenceProvider = DevelopmentContext.this.persistenceProvider
  }
}
