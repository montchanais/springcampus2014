/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.context

/**
 * Keeps track of the runtime environment in which the SpringCampus demonstration is run.
 */
object Environment {

  /**
   * Returns the current environment.
   */
  val current: EnvironmentType.EnvironmentType = EnvironmentType.Dev  // hardcoded to DEV - at a later stage this needs to be dynamic

  /**
   * Returns the current application context (based on the current runtime environment).
   */
  lazy val context: Context =
    current match {
      case EnvironmentType.Dev => new DevelopmentContext()
      case _ => throw new IllegalStateException( s"Environment type '${current}' is not supported." )
    }
}

object EnvironmentType extends Enumeration {
  type EnvironmentType = Value
  val Dev, Prod = Value
}
