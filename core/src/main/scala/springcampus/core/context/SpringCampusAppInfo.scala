/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.context

object SpringCampusAppInfo extends AppInfo {

  val appName: String = "CERN SpringCampus 2014 Gijón (Spain) - Scala & Play Hands-On"

  val appShortName: String = "springcampus"

  val appVersion: String = "v1.0-SNAPSHOT"

  val appProjectLink: String = "https://bitbucket.org/montchanais/springcampus2014"

  val appCopyrightYears: String = "2014"

  val appCopyrightOrg: String = "Jan Janke (CERN)"

  val appCopyright: String = s"Copyright (c) $appCopyrightYears $appCopyrightOrg ($appProjectLink)"
}
