/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.reflect.runtime.{universe => ru}
import scala.Some
import scala.collection.mutable

private[db] object DomainUtils {

  private val _cacheCollections = new mutable.HashMap[ru.Type,String]()

  /**
   * Returns the name of the collection to use when persisting retrieving objects of the given type (which must
   * always be a sub type of [[springcampus.core.db.MDomain]]).
   *
   * @param of the type to be checked
   * @return the name of the collection to be used
   */
  def collectionName( of: ru.Type ): String = _cacheCollections.getOrElse( of, {
      val coll = findBaseDomain( of )
      _cacheCollections += of -> coll
      coll
    }
  )

  /**
   * Returns the name of the discriminator to use when persisting/retrieving objects of the given type (which must
   * always be a sub type of [[springcampus.core.db.MDomain]]). The discriminator is always the simple name of the concrete
   * final non-abstract implementation of `TDomain`.
   *
   * @param of the type to be checked
   * @return the name of the discriminator to be used
   */
  def discriminatorName( of: ru.Type ): String = of.typeSymbol.name.encoded

  /**
   * Finds the closest descendant of [[springcampus.core.db.MDomain]] that is a super class of the given domain type.
   *
   * @param of the type for which to find the closest super type that is not `TDomain`
   */
  def findBaseDomain( of: ru.Type ): String = {
    val optBaseDomain = findClosestAncestor( of.typeSymbol.asClass, ru.typeOf[MDomain] )
    if ( optBaseDomain == None )
      throw new IllegalArgumentException( s"The type ${of.typeSymbol.fullName} is not a sub-type of ${ru.typeOf[MDomain].typeSymbol.fullName}." )

    // return the simple name (findClosestAncestor returns a fully qualified class name)
    //
    optBaseDomain.get.substring( optBaseDomain.get.lastIndexOf( '.' ) + 1 )
  }

  /**
   * Returns the name of the first super trait implementing/extending a given Class.
   *
   * @param of the symbol representing the descendant for which to find the farest ancestor not being `below`
   * @param below the type of which the closest descendant that is a super class/trait of `of`
   * @param descendant the symbol representing the descendant of the currently inspected class (only used internally for
   *                   recursive calls)
   * @return the fully qualified name of the farest ancestor directly below `below` or `None` if this class is not
   *         part of a class hierarchy below `below`
   */
  private def findClosestAncestor( of: ru.ClassSymbol, below: ru.Type, descendant: Option[ru.ClassSymbol] = None ): Option[String] = {
    val nameObject = ru.typeOf[Object].typeSymbol.fullName
    val nameBelow = below.typeSymbol.fullName

    if ( of.fullName == nameObject )
      return None
    else if ( of.fullName == nameBelow )
      if ( descendant == None )
        None // TDomain itself was checked but not a descendant
      else
        Some( descendant.get.fullName )
    else {
      // try ancestors and check if it is based on TDomain
      // (the method ClassSymbol.baseClasses always returns itself as head, therefore we only look at the tail)
      //
      for ( ancestor <- of.baseClasses.tail ) {
        val result = findClosestAncestor( ancestor.asClass, below, Some( of ) )
        if ( result != None )
          return result
      }

      return None
    }
  }
}
