/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.reflect.runtime.{universe => ru}

/**
 * Mandatory base trait for all domain classes.
 */
trait MDomain {

  /** Unique instance identifier */
  val id: MId

  /** List of all persistable attributes. Does not need to contain mandatory attributes like `id`. */
  val attributes: Map[Symbol,Any]

  /** Returns a reference to this domain object. */
  def ref: MRef[_]

  /**
   * Returns all the attributes that are suitable to be exposed to a UI.
   */
  lazy val attrsUiFullVersion: Map[Symbol, Any] = Map( 'id -> id ) ++ MDomain.this.attributes

  /**
   * Returns the minimum number of attributes that are suitable to be exposed to a UI (especially intended for
   * objects that are embedded into other objects). By default corresponds to the full version.
   */
  lazy val attrsUiMiniVersion: Map[Symbol, Any] = attrsUiFullVersion

  /** The actual implementation type */
  final protected lazy val implType: ru.Type = ru.runtimeMirror( getClass.getClassLoader ).classSymbol( getClass ).asType.toType

  /** Name of the referenced final non-abstract type. */
  final lazy val discriminator: String = DomainUtils.discriminatorName( implType )

  /** Name of the collection to be used for storing objects of the referenced type. */
  final lazy val collection: String = DomainUtils.collectionName( implType )

  /** Returns a String in the form 'SimpleClassName(id)'. */
  final lazy val toShortString: String = s"$discriminator($id)"

  /** Returns a MongoObject representing this TDomain. */
  final def toMongo = MongoObject( Map( MongoObject.ID -> id, MongoObject.DISCRIMINATOR -> discriminator ) ++ attributes )
}
