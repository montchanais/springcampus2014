/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.reflect.runtime.{universe => ru}

/**
 * Base class for all concrete domain classes' companion objects.
 *
 * @tparam A represents the class for which this object acts as companion
 */
abstract class MDomainCompanion[A <: MDomain : ru.TypeTag] {

  def fromMongo( mo: MongoObject, dcr: MDomainCompanionRegistry ): A

  def ref( refid: MId ): MRef[A] = new MRef[A]( refid )

  def ref( domain: A ): MRef[A] = ref( domain.id )

  /** Name of the referenced final non-abstract type. */
  final lazy val discriminator: String = DomainUtils.discriminatorName( ru.typeOf[A] )

  /** Name of the collection to be used for storing objects of the referenced type. */
  final lazy val collection: String = DomainUtils.collectionName( ru.typeOf[A] )
}
