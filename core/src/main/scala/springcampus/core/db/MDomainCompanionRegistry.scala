/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

/**
 * Registry of all [[springcampus.core.db.MDomainCompanion]] objects.
 */
trait MDomainCompanionRegistry {

  /**
   * Map of the simple names of all Domain objects to their matching DomainCompanions.
   */
  protected val domainCompanionsByName: Map[String, MDomainCompanion[_]]

  /**
   * Returns the [[springcampus.core.db.MDomainCompanion]] object that matches the given domain name.
   *
   * @param domainName the simple name of the Domain class for which to obtain the companion object
   * @return the matching companion object
   * @throws IllegalArgumentException if no DomainCompanion matching the given domain name is registered
   */
  def domainCompanion( domainName: String ): MDomainCompanion[_] =
    domainCompanionsByName.getOrElse( domainName, { throw new IllegalArgumentException( s"MDomainCompanion for domain $domainName is not registered." ) } )

  /**
   * Creates a Key object for the given domain name and the ID.
   *
   * @param domainName the name of the Domain for which to create a Key
   * @param id the ID of the Key
   * @return the created Key object
   * @throws IllegalArgumentException if the given domain name does not refer to any registered DomainCompanion object
   */
  def createRef( domainName: String, id: MId ): MRef[_] = domainCompanion( domainName ).ref( id )
}
