/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

/**
 * Marks a domain class as usable as an 'embedded' TDomain. I.e. instances of this trait cannot be persisted directly
 * as they do not have an ID or key but they can be embedded into other TDomain objects.
 */
trait MEmbeddedDomain {
  val attributes: Map[Symbol,Any]

  /**
   * Returns all the attributes that are suitable to be exposed to a UI.
   */
  lazy val attrsUiFullVersion: Map[Symbol, Any] = MEmbeddedDomain.this.attributes

  /** Returns the object in which all MId or MRef values are resolved as strings. */
  lazy val withResolvedIds: MEmbeddedDomain = new MEmbeddedDomain {
    val attributes: Map[Symbol, Any] = MEmbeddedDomain.this.attributes.map{
      case (name,value) => {
        value match {
          case i:MId => name -> i.toString
          case r:MRef[_] => name -> r.refId.toString
          case _ => name -> value
        }
      }
    }
  }

  /** Returns a MongoObject representing this TDomain. */
  final def toMongo = MongoObject( attributes )
}
