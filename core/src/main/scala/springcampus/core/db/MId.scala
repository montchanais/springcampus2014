/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import org.bson.types.ObjectId

sealed abstract class MId {
  type IdType

  val value: IdType

  def asNameId: MNameId = throw new IllegalStateException( s"This ID is not a NameId, but a ${getClass.getName}." )
  def asNumericId: MNumericId = throw new IllegalStateException( s"This ID is not a NumericId, but a ${getClass.getName}." )
  def asBinaryId: MBinaryId = throw new IllegalStateException( s"This ID is not a BinaryId, but a ${getClass.getName}." )

  def toBSON: AnyRef

  def toBSONString: String

  override def toString: String = value.toString

  override def hashCode(): Int = value.hashCode()

  override def equals( other: scala.Any ): Boolean =
    other match {
      case that: MId => that.canEqual( this ) && that.value.equals( this.value )
      case _ => false
    }

  protected def canEqual( other: Any ): Boolean = other.isInstanceOf[MId]
}

object MId{
  def apply( id: Any ): MId = id match {
    case s: String => new MNameId( s )
    case l: Long => new MNumericId( l )
    case o: ObjectId => new MBinaryId( Some( o ) )
    case _ => throw new IllegalArgumentException( s"The value $id has an unsupported type (${id.getClass.getName})." )
  }

  // convenience quick ID creation methods
  //
  def name( name: String ): MNameId = new MNameId( name )
  def num( num: Long ): MNumericId = new MNumericId( num )
  def bin( bin: Option[ObjectId] = None ): MBinaryId = new MBinaryId( bin )
}

class MNameId( val value: String ) extends MId {
  type IdType = String

  def toBSON = value

  override def asNameId = this

  override def toBSONString: String = '\"' + value + '\"'
}

class MNumericId( val value: Long ) extends MId {
  type IdType = Long

  def toBSON = Long.box( value )

  override def asNumericId = this

  override def toBSONString: String = s"$value"
}

class MBinaryId( _value: Option[ObjectId] = None ) extends MId {
  type IdType = ObjectId

  def toBSON = value

  val value = if ( _value == None ) new ObjectId() else _value.get

  override def asBinaryId = this

  override def toBSONString: String = "ObjectId(\"" + value.toString + "\")"
}
