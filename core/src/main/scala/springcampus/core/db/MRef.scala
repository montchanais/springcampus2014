/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.reflect.runtime.{universe => ru}
import org.bson.types.ObjectId

/**
 * Typed reference (foreign key) to another entity.
 *
 * @param refId the `Id` of the referenced `TDomain` instance
 * @tparam A the `TDomain` class which is referenced by an instance of this class.
 */
final class MRef[+A <: MDomain : ru.TypeTag] ( val refId: MId ) {

  // Make sure that this class is always instantiated with a concrete type parameter
  //
  assume( ru.typeOf[A].typeSymbol.fullName != "scala.Nothing", s"No valid type argument has been specified when creating MRef for refId '$refId'." )

  /** Name of the referenced final non-abstract type. */
  lazy val discriminator: String = DomainUtils.discriminatorName( ru.typeOf[A] )

  /** Name of the collection to be used for storing objects of the referenced type. */
  lazy val collection: String = DomainUtils.collectionName( ru.typeOf[A] )

  /** Returns a MongoObject representing this TDomain. */
  final def toMongo = MongoObject(
    Map(
      MongoObject.DISCRIMINATOR -> discriminator,
      MongoObject.COLLECTION -> collection,
      MongoObject.REF_ID -> refId
    )
  )

  override def toString: String = s"MRef[$discriminator]($refId)"

  // calculate hashcode according to recipe in "Programming in Scala, 2nd Edition" by Odersky, Spoon, Venners
  override def hashCode(): Int =
    41 * (
      41 * (
        41 * (
          discriminator.hashCode + 41
        ) + collection.hashCode
      ) + refId.hashCode()
    )

  // actually not needed in final classes - but for the sake of safety in possible future enhancement, it is here
  protected def canEqual( other: Any ): Boolean = other.isInstanceOf[MRef[_]]

  override def equals( other: scala.Any ): Boolean =
    other match {
      case that: MRef[_] =>
        that.canEqual( this ) &&
        discriminator == that.discriminator &&
        collection == that.collection &&
        refId == that.refId
      case _ => false
    }
}

object MRef {
  def fromDomains[A <: MDomain : ru.TypeTag]( domains: List[A] ): List[MRef[A]] = domains.map( _.ref.asInstanceOf[MRef[A]] )

  def fromName[A <: MDomain : ru.TypeTag]( name: String ): MRef[A] = new MRef[A]( MId.name( name ) )

  def fromNames[A <: MDomain : ru.TypeTag]( ids: List[String] ): List[MRef[A]] = ids.map( id => new MRef[A]( MId.name( id ) ) )

  def fromNums[A <: MDomain : ru.TypeTag]( ids: List[Long] ): List[MRef[A]] = ids.map( id => new MRef[A]( MId.num( id ) ) )

  def fromBins[A <: MDomain : ru.TypeTag]( ids: List[ObjectId] ): List[MRef[A]] = ids.map( id => new MRef[A]( MId.bin( Some( id ) ) ) )
}
