/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import com.mongodb.{DBObject, BasicDBList, BasicDBObject}
import scala.collection.JavaConversions._
import org.joda.time.DateTime
import scala.Some
import org.bson.types.ObjectId
import java.util.Date

/** Represents a MongoElement (an element that can be saved within a MongoDB collection). */
sealed trait MongoElement {

  def isObject = false
  def isArray = false
  def isLiteral = false
  def isNull = false
  def isString = false
  def isInt = false
  def isLong = false
  def isDouble = false
  def isBoolean = false
  def isDateTime = false
  def isObjectId = false

  def asObject: MongoObject = throw new UnsupportedOperationException
  def asArray: MongoArray = throw new UnsupportedOperationException
  def asLiteral: MongoLiteral = throw new UnsupportedOperationException
  def asNull: MongoNull = throw new UnsupportedOperationException
  def asString: MongoString = throw new UnsupportedOperationException
  def asInt: MongoInt = throw new UnsupportedOperationException
  def asLong: MongoLong = throw new UnsupportedOperationException
  def asDouble: MongoDouble = throw new UnsupportedOperationException
  def asBoolean: MongoBoolean = throw new UnsupportedOperationException
  def asDateTime: MongoDateTime = throw new UnsupportedOperationException
  def asObjectId: MongoObjectId = throw new UnsupportedOperationException

  final def toBSON: AnyRef = this match {
    case o: MongoObject => o.toDBObject
    case a: MongoArray => a.toDBArray
    case l: MongoLiteral => l.toDBValue
    case _ => throw new IllegalArgumentException( s"Unexpected element type ${getClass.getName}." )
  }
}

/**
 * Wraps a MongoDB BasicDBObject and adds a few convenience methods (e.g. to handle dates as Joda Time etc.).
 */
class MongoObject private ( private val _members: Map[Symbol,MongoElement] ) extends MongoElement {

  override def isObject = true

  override def asObject: MongoObject = this

  def toDBObject: DBObject = {
    val dbo = new BasicDBObject( _members.size )
    _members.foreach{ case ( key, element ) => dbo.append( key.name, element.toBSON ) }
    dbo
  }

  def id: MId = {
    MId(
      _members.getOrElse(
        MongoObject.ID,
        throw new IllegalStateException( s"MongoObject has no ${MongoObject.ID.name} attribute." )
      ).asLiteral.value
    )
  }

  def refId: MId = {
    MId(
      _members.getOrElse(
        MongoObject.REF_ID,
        throw new IllegalStateException( s"MongoObject has no ${MongoObject.REF_ID.name} attribute." )
      ).asLiteral.value
    )
  }

  def discriminator: String = {
    _members.getOrElse(
      MongoObject.DISCRIMINATOR,
      throw new IllegalStateException( s"MongoObject has no ${MongoObject.DISCRIMINATOR.name} attribute." )
    ).asString.value
  }

  def collection: String = {
    _members.getOrElse(
      MongoObject.COLLECTION,
      throw new IllegalStateException( s"MongoObject has no ${MongoObject.COLLECTION.name} attribute." )
    ).asString.value
  }

  def attrIsNull( name: Symbol ): Boolean = !_members.contains( name ) || _members.get( name ).get.isNull

  def attrString( name: Symbol ): String = nullSafeGet( name ).asString.value
  def attrStringOption( name: Symbol ): Option[String] = if ( attrIsNull( name ) ) None else Some( attrString( name ) )

  def attrInt( name: Symbol ): Int = nullSafeGet( name ).asInt.value
  def attrIntOption( name: Symbol ): Option[Int] = if ( attrIsNull( name ) ) None else Some( attrInt( name ) )

  def attrLong( name: Symbol ): Long = nullSafeGet( name ).asLong.value
  def attrLongOption( name: Symbol ): Option[Long] = if ( attrIsNull( name ) ) None else Some( attrLong( name ) )

  def attrDouble( name: Symbol ): Double = nullSafeGet( name ).asDouble.value
  def attrDoubleOption( name: Symbol ): Option[Double] = if ( attrIsNull( name ) ) None else Some( attrDouble( name ) )

  def attrBoolean( name: Symbol ): Boolean = nullSafeGet( name ).asBoolean.value
  def attrBooleanOption( name: Symbol ): Option[Boolean] = if ( attrIsNull( name ) ) None else Some( attrBoolean( name ) )

  def attrDateTime( name: Symbol ): DateTime = nullSafeGet( name ).asDateTime.value
  def attrDateTimeOption( name: Symbol ): Option[DateTime] = if ( attrIsNull( name ) ) None else Some( attrDateTime( name ) )

  def attrObject( name: Symbol ): MongoObject = nullSafeGet( name ).asObject
  def attrObjectOption( name: Symbol ): Option[MongoObject] = if ( attrIsNull( name ) ) None else Some( attrObject( name ) )

  def attrRef[A <: MDomain]( name: Symbol, dcr: MDomainCompanionRegistry ): MRef[A] = {
    val mo = attrObject( name )
    dcr.createRef( mo.discriminator, mo.refId ).asInstanceOf[MRef[A]]
  }

  def attrRefOption[A <: MDomain]( name: Symbol, dcr: MDomainCompanionRegistry ): Option[MRef[A]] = {
    if ( attrIsNull( name ) )
      None
    else
      Some( attrRef( name, dcr ) )
  }

  def attrArray( name: Symbol ) = nullSafeGet( name ).asArray

  /**
   * Returns the given attribute as Map[A,B].
   *
   * @param memberName the attribute which is supposed to reference a Map
   * @param fnMap a function which translates a JSON attribute name/value pair into key/value pair matching the requested Map types
   * @tparam A the type of the Map keyCombination
   * @tparam B the type of the Map values
   * @return a new Map[A,B]
   */
  def attrMap[A,B]( memberName: Symbol, fnMap: (Symbol,MongoElement) => (A,B) ): Map[A,B] =
    attrObject( memberName )._members.map{ case ( attr, value ) => fnMap( attr, value ) }

  private def nullSafeGet( name: Symbol ): MongoElement = {
    _members.getOrElse( name, throw new PersistenceException( s"No attribute with the name $name exists." ) )
  }
}

object MongoObject {

  private[db] val ID = '_id
  private[db] val DISCRIMINATOR = 'discriminator
  private[db] val COLLECTION = 'collection
  private[db] val REF_ID = 'refid

  def apply( members: Map[Symbol,Any] ): MongoObject = {
    new MongoObject(
      members.map{
        case ( attrName, value ) => attrName -> createMongoElement( value )
      }
    )
  }

  def createMongoElement( value: Any ): MongoElement = {
    def transformMapKey( key: Any ): Symbol = key match {
      case s: Symbol => s
      case s: String => Symbol( s )
      case r: MRef[_] => Symbol( r.refId.toString )
      case _ => throw new PersistenceException( s"Map key type ${key.getClass.getName} is not supported." )
    }

    value match {
      case null => new MongoNull()
      case None => new MongoNull()
      case si: MNameId => new MongoString( si.value )
      case ni: MNumericId => new MongoLong( ni.value )
      case bi: MBinaryId => new MongoObjectId( bi.value )
      case m: MongoElement => m
      case s: Some[_] => createMongoElement( s.get )
      case s: String => new MongoString( s )
      case d: Double => new MongoDouble( d )
      case i: Int => new MongoInt( i )
      case l: Long => new MongoLong( l )
      case b: Boolean => new MongoBoolean( b )
      case d: DateTime => new MongoDateTime( d )
      case d: Date => new MongoDateTime( new DateTime( d.getTime ) )
      case d: MDomain => d.toMongo
      case e: MEmbeddedDomain => e.toMongo
      case r: MRef[_] => r.toMongo
      case l: List[_] => MongoArray( l )
      case m: Map[_,_] => MongoObject( m.map{ case ( key, value ) => transformMapKey( key ) -> value } )
      case ar: BasicDBList => MongoArray.fromBSON( ar )
      case db: DBObject => fromBSON( db )
      case oi: ObjectId => new MongoObjectId( oi )
      case _ => throw new PersistenceException( s"Element $value has an unsupported Mongo BSON type." )
    }
  }

  def fromBSON( bson: DBObject ): MongoObject = {
    MongoObject(
      bson.toMap.map{
        case ( key, value ) => Symbol( key.toString ) -> value
      }.toMap
    )
  }

  /**
   * Returns the Mongo attribute name of the given MRef attribute name.
   *
   * @param refName the name of the MRef attribute
   * @return the internal Mongo specific name of the MRef attribute
   */
  def refAttr( refName: String ): String = s"$refName.${MongoObject.REF_ID.name}"
}

class MongoArray ( private val _elements: List[MongoElement] ) extends MongoElement {

  override def isArray = true

  override def asArray: MongoArray = this

  def toDBArray: BasicDBList = {
    val dba = new BasicDBList()
    dba.addAll( _elements.map( _.toBSON ) )
    dba
  }

  def toListObject: List[MongoObject] = _elements.map(
    el => {
      el match {
        case n: MongoNull => null
        case o: MongoObject => o
        case _ => throw new PersistenceException( s"Element $el is not a MongoObject or null." )
      }
    }
  )

  def toListString: List[String] = _elements.map(
    el => {
      el match {
        case n: MongoNull => null
        case s: MongoString => s.value
        case _ => throw new PersistenceException( s"Element $el is not a MongoString or null." )
      }
    }
  )

  def toListInt: List[Int] = _elements.map(
    el => {
      el match {
        case n: MongoNull => 0
        case i: MongoInt => i.value
        case _ => throw new PersistenceException( s"Element $el is not a MongoInt or null." )
      }
    }
  )

  def toListLong: List[Long] = _elements.map(
    el => {
      el match {
        case n: MongoNull => 0L
        case i: MongoLong => i.value
        case _ => throw new PersistenceException( s"Element $el is not a MongoLong or null." )
      }
    }
  )

  def toListDouble: List[Double] = _elements.map(
    el => {
      el match {
        case n: MongoNull => 0.0d
        case d: MongoDouble => d.value
        case _ => throw new PersistenceException( s"Element $el is not a MongoDouble or null." )
      }
    }
  )

  def toListBoolean: List[Boolean] = _elements.map(
    el => {
      el match {
        case n: MongoNull => false
        case d: MongoBoolean => d.value
        case _ => throw new PersistenceException( s"Element $el is not a MongoBoolean or null." )
      }
    }
  )

  def toListRef[A <: MDomain]( dcr: MDomainCompanionRegistry ): List[MRef[A]] = _elements.map(
    el => {
      el match {
        case o: MongoObject => dcr.createRef( o.discriminator, o.refId ).asInstanceOf[MRef[A]]
        case _ => throw new PersistenceException( s"Element $el is not a MongoObject." )
      }
    }
  )

  def length: Int = _elements.length

  def iterator: Iterator[MongoElement] = _elements.iterator
}

object MongoArray {
  def apply( elements: List[Any] ): MongoArray = {
    new MongoArray( elements.map( MongoObject.createMongoElement( _ ) ) )
  }

  def fromBSON( bson: BasicDBList ): MongoArray = MongoArray( bson.toList )
}

abstract class MongoLiteral extends MongoElement {
  type LiteralType

  val value: LiteralType

  def toDBValue: AnyRef

  override def isLiteral = true

  override def asLiteral: MongoLiteral = this
}

final class MongoNull extends MongoLiteral {
  type LiteralType = Null

  val value = null

  def toDBValue = value

  override def isNull = true

  override def asNull = this
}

final class MongoString ( val value: String ) extends MongoLiteral {
  type LiteralType = String

  def toDBValue = value

  override def isString = true

  override def asString = this
}

final class MongoInt ( val value: Int ) extends MongoLiteral {
  type LiteralType = Int

  def toDBValue = Int.box( value )

  override def isInt = true

  override def asInt = this
}

final class MongoLong ( val value: Long ) extends MongoLiteral {
  type LiteralType = Long

  def toDBValue = Long.box( value )

  override def isLong = true

  override def asLong = this
}

final class MongoDouble ( val value: Double ) extends MongoLiteral {
  type LiteralType = Double

  def toDBValue = Double.box( value )

  override def isDouble = true

  override def asDouble = this
}

final class MongoBoolean ( val value: Boolean ) extends MongoLiteral {
  type LiteralType = Boolean

  def toDBValue = Boolean.box( value )

  override def isBoolean = true

  override def asBoolean = this
}

final class MongoDateTime ( val value: DateTime ) extends MongoLiteral {
  type LiteralType = DateTime

  def toDBValue = value.toDate

  override def isDateTime = true

  override def asDateTime = this
}

final class MongoObjectId ( val value: ObjectId ) extends MongoLiteral {
  type LiteralType = ObjectId

  def toDBValue = value

  override def isObjectId = true

  override def asObjectId = this
}
