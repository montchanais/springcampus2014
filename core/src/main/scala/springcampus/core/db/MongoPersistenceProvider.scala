/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.collection.JavaConversions._
import com.mongodb._
import com.typesafe.scalalogging.log4j.Logging
import scala.collection.mutable.ListBuffer
import scala.reflect.runtime.{universe => ru}

class MongoPersistenceProvider( protected val domainCompanionRegistry: MDomainCompanionRegistry,
                                protected val nodes: List[ServerAddress],
                                protected val database: String,
                                protected val userPassword: Option[(String,String)] = None ) extends PersistenceProvider with Logging {

  import springcampus.core.misc.GeneralUtils._

  private[this] var _isClientInitialized: Boolean = false

  protected lazy val mongoClient: MongoClient = {
    val options = MongoClientOptions.builder().build()
    val tmp = new MongoClient( nodes, options )
    tmp.setWriteConcern( WriteConcern.ACKNOWLEDGED.continueOnErrorForInsert( false ) )
    logger.info( "operation=createMongoClient; status=done;" )
    _isClientInitialized = true
    tmp
  }

  protected lazy val db: DB = {
    val tmp = mongoClient.getDB( database )
    if ( userPassword != None )
      if ( !tmp.authenticate( userPassword.get._1, userPassword.get._2.toCharArray ) )
        logger.error( "operation=authenticateMongoDB; status=failed; msg=authentication to mongo database {} failed", database )

    logger.info( "operation=connectToDB; status=done; db={};", database )
    tmp
  }

  def shutdown() {
    if ( _isClientInitialized )
      mongoClient.close()
  }

  def deleteAll() {

    val opResult = timedOperation[Unit]( true ){
      db.dropDatabase()
    }

    logger.info( "operation=dropDB; time={}ms;", opResult.duration.toString )
  }

  def insert[A <: MDomain]( domain: A ): A = {

    val opResult = timedOperation[WriteResult]( logger.underlying.isTraceEnabled ){
      val coll = db.getCollection( domain.collection )
      coll.insert( domain.toMongo.toDBObject )
    }

    val commandResult = opResult.result.getLastError

    logger.trace( "operation=insert; time={}ms; ok={}; domain={};", opResult.duration.toString, commandResult.ok().toString, domain.toShortString )

    checkResult( "insert", commandResult, Some( domain ) )

    domain
  }

  def insert[A <: MDomain]( domains: List[A] ): List[A] = {

    if ( domains.isEmpty )
      domains;
    else {
      val opResult = timedOperation[WriteResult]( logger.underlying.isTraceEnabled ){
        val coll = db.getCollection( domains.head.collection )
        coll.insert( domains.map( _.toMongo.toDBObject ) )
      }

      val commandResult = opResult.result.getLastError

      logger.trace( "operation=insertBulk; time={}ms; ok={}; domainType={};", opResult.duration.toString, commandResult.ok().toString, domains.head.collection )

      checkResult( "insertBulk", commandResult )

      domains
    }
  }

  def update[A <: MDomain]( domain: A ): A = {

    val opResult = timedOperation[WriteResult]( logger.underlying.isTraceEnabled ){
      val coll = db.getCollection( domain.collection )
      coll.update( new BasicDBObject( MongoObject.ID.name, domain.id.toBSON ), domain.toMongo.toDBObject, false, false )
    }

    val commandResult = opResult.result.getLastError

    logger.trace( "operation=update; time={}ms; ok={}; domain={};", opResult.duration.toString, commandResult.ok().toString, domain.toShortString )

    checkResult( "update", commandResult, Some( domain ) )

    domain
  }

  def update[A <: MDomain]( domains: List[A] ): List[A] = {
    domains.foreach( update( _ ) )
    domains
  }

  def save[A <: MDomain]( domain: A ): A = {

    val opResult = timedOperation[WriteResult]( logger.underlying.isTraceEnabled ){
      val coll = db.getCollection( domain.collection )
      coll.save( domain.toMongo.toDBObject )
    }

    val commandResult = opResult.result.getLastError

    logger.trace( "operation=save; time={}ms; ok={}; domain={};", opResult.duration.toString, commandResult.ok().toString, domain.toShortString )

    checkResult( "save", commandResult, Some( domain ) )

    domain
  }

  def save[A <: MDomain]( domains: List[A] ): List[A] = {
    domains.foreach( save( _ ) )
    domains
  }

  def findById[A <: MDomain : ru.TypeTag]( id: MId ): Option[A] = {

    val opResult = timedOperation[Option[A]]( logger.underlying.isTraceEnabled ){
      val coll = db.getCollection( DomainUtils.collectionName( ru.typeOf[A] ) )
      val cursor = coll.find( new BasicDBObject( MongoObject.ID.name, id.toBSON ) )
      withResource( cursor ) {
        if ( cursor.hasNext )
          Some( instantiateDomain( MongoObject.fromBSON( cursor.next() ) ) )
        else
          None
      }
    }

    logger.trace( "operation=findById; time={}ms; id={}; result={};", opResult.duration.toString, id, opResult.result.toString )

    opResult.result
  }

  def findByIds[A <: MDomain : ru.TypeTag]( ids: List[MId], orderBy: (String,OrderDir.OrderDir)* ): List[A] = {
    val mq = MQBuilder[A]().add( MQCriteria.in( MongoObject.ID.name )( ids ) ).orderBy( orderBy ).build
    findByQuery( mq )
  }

  def findByRefAttr[A <: MDomain : ru.TypeTag]( refName: String, refId: MId, orderBy: (String,OrderDir.OrderDir)* ): List[A] = {
    val mq = MQBuilder[A]().add( MQCriteria.eq( MongoObject.refAttr( refName ) )( refId ) ).orderBy( orderBy ).build
    findByQuery( mq )
  }

  def findByRefAttr[A <: MDomain : ru.TypeTag]( refName: String, refIds: List[MId], orderBy: (String,OrderDir.OrderDir)* ): List[A] = {
    val mq = MQBuilder[A]().add( MQCriteria.in( MongoObject.refAttr( refName ) )( refIds ) ).orderBy( orderBy ).build
    findByQuery( mq )
  }

  def findByIdOrFail[A <: MDomain : ru.TypeTag]( id: MId ): A =
    findById[A]( id ).getOrElse{ throw new PersistenceException( s"No object with id '$id' exists." ) }

  def findAll[A <: MDomain : ru.TypeTag](): List[A] = {
    val typeA = ru.typeOf[A]
    val collectionName = DomainUtils.collectionName( typeA )
    val discriminatorName = DomainUtils.discriminatorName( typeA )

    if ( collectionName != discriminatorName ) {
      // If the discriminator is different than the collection, we have to construct a query with a criteria on the
      // discriminator to only return instances matching the given exact type.
      //
      findByQuery[A]( MQBuilder[A].build, discriminatorName )
    }
    else {
      val opResult = timedOperation[List[A]]( logger.underlying.isTraceEnabled ){
        val coll = db.getCollection( collectionName )
        val cursor = coll.find()
        withResource( cursor ) {
          val buffer = ListBuffer.empty[A]
          while ( cursor.hasNext )
            buffer += instantiateDomain( MongoObject.fromBSON( cursor.next() ) )
          buffer.toList
        }
      }

      logger.trace( "operation=findAll; time={}ms; numObjects={};", opResult.duration.toString, opResult.result.size.toString )

      opResult.result
    }
  }

  def findByQuery[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): List[A] =
    findByQuery( query, DomainUtils.discriminatorName( ru.typeOf[A] ) )

  /**
   * Executes the given query. Before executing, the method checks whether the discriminator matches the collection
   * underlying the given query. If not, an additional criterion to only return instances matching the given
   * discriminator is added to the query before execution.
   * 
   * @param query the query to be executed
   * @param discriminator the discriminator of the queried domain
   * @tparam A the type of the domain class that is searched
   * @return a List of objects returned by the query (may be empty)
   */
  private def findByQuery[A <: MDomain]( query: MongoQuery[A], discriminator: String ): List[A] = {
    // Add an additional condition on the discriminator if it is different from the query's underlying collection.
    // This is necessary to only return instances that match the actual domain type.
    //
    val queryInternal =
      if ( query.collection != discriminator )
        query.addCriterion( MQCriteria.eq( MongoObject.DISCRIMINATOR.name )( discriminator ) )
      else
        query

    logger.debug( "operation=findByQuery; query=[{}];", queryInternal.toString )

    val coll = db.getCollection( queryInternal.collection )
    val opResult = timedOperation[List[A]]( logger.underlying.isTraceEnabled ){
      val cursor = prepareDBCursor( coll, queryInternal )
      withResource( cursor ) {
        val buffer = ListBuffer.empty[A]
        while ( cursor.hasNext )
          buffer += instantiateDomain( MongoObject.fromBSON( cursor.next() ) )
        buffer.toList
      }
    }

    logger.trace( "operation=findByQuery; collection={}; time={}ms; numObjects={};", coll.getName, opResult.duration.toString, opResult.result.size.toString )

    opResult.result
  }

  def findOneByQuery[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): Option[A] =
    findOneByQuery( query, DomainUtils.discriminatorName( ru.typeOf[A] ) )

  /**
   * Executes the given query. Before executing, the method checks whether the discriminator matches the collection
   * underlying the given query. If not, an additional criterion to only return instances matching the given
   * discriminator is added to the query before execution.
   *
   * @param query the query to be executed
   * @param discriminator the discriminator of the queried domain
   * @tparam A the type of the domain class that is searched
   * @return the first object returned by the underlying query (if any)
   */
  private def findOneByQuery[A <: MDomain]( query: MongoQuery[A], discriminator: String  ): Option[A] = {
    // Add an additional condition on the discriminator if it is different from the query's underlying collection.
    // This is necessary to only return instances that match the actual domain type.
    //
    val queryInternal =
      if ( query.collection != discriminator )
        query.addCriterion( MQCriteria.eq( MongoObject.DISCRIMINATOR.name )( discriminator ) )
      else
        query

    logger.debug( "operation=findOneByQuery; query=[{}];", queryInternal.toString )

    val coll = db.getCollection( queryInternal.collection )
    val opResult = timedOperation[Option[A]]( logger.underlying.isTraceEnabled ){
      val obj = coll.findOne( queryInternal.toBSON )
      if ( obj == null )
        None
      else
        Some( instantiateDomain( MongoObject.fromBSON( obj ) ) )
    }

    logger.trace( "operation=findOneByQuery; collection={}; time={}ms; numObjects={};", coll.getName, opResult.duration.toString, opResult.result )

    opResult.result
  }

  def findOneByQueryOrFail[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): A =
    findOneByQuery[A]( query ).getOrElse{ throw new PersistenceException( s"No objects match the query '${query.toString}'." ) }

  def countAll[A <: MDomain : ru.TypeTag](): Long = {
    val typeA = ru.typeOf[A]
    val collectionName = DomainUtils.collectionName( typeA )
    val discriminatorName = DomainUtils.discriminatorName( typeA )

    if ( collectionName != discriminatorName ) {
      // If the discriminator is different than the collection, we have to construct a query with a criteria on the
      // discriminator to only return instances matching the given exact type.
      //
      count[A]( MQBuilder[A].build, discriminatorName )
    }
    else {
      val coll = db.getCollection( DomainUtils.collectionName( ru.typeOf[A] ) )
      val opResult = timedOperation[Long]( logger.underlying.isTraceEnabled ){
        coll.count()
      }

      logger.trace( "operation=countAll; collection={}; time={}ms; numObjects={};", coll.getName, opResult.duration.toString, opResult.result.toString )

      opResult.result
    }
  }

  def count[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): Long =
    count( query, DomainUtils.discriminatorName( ru.typeOf[A] ) )

  /**
   * Counts the number of objects returned by the given query. Before executing, the method checks whether the
   * discriminator matches the collection underlying the given query. If not, an additional criterion to only
   * return instances matching the given discriminator is added to the query before execution.
   *
   * @param query the query to be executed
   * @param discriminator the discriminator of the queried domain
   * @tparam A the type of the domain class that is searched
   * @return the first object returned by the underlying query (if any)
   */
  private def count[A <: MDomain : ru.TypeTag]( query: MongoQuery[A], discriminator: String ): Long = {
    // Add an additional condition on the discriminator if it is different from the query's underlying collection.
    // This is necessary to only count instances that match the actual domain type.
    //
    val queryInternal =
      if ( query.collection != discriminator )
        query.addCriterion( MQCriteria.eq( MongoObject.DISCRIMINATOR.name )( discriminator ) )
      else
        query

    logger.debug( "operation=count; query=[{}];", queryInternal.toString )

    val coll = db.getCollection( queryInternal.collection )
    val opResult = timedOperation[Long]( logger.underlying.isTraceEnabled ){
      val cursor = prepareDBCursor( coll, queryInternal )
      withResource( cursor ) {
        cursor.count()
      }
    }

    logger.trace( "operation=count; collection={}; time={}ms; numObjects={};", coll.getName, opResult.duration.toString, opResult.result.toString )

    opResult.result
  }

  def seqNextValue( seqName: Option[String] = None ): MNumericId = {
    val coll = db.getCollection( "sequences" )

    // create sequence if it does not yet exist
    //
    coll.insert( new BasicDBObject( MongoObject.ID.name, seqName.getOrElse( "global" ) ).append( "seq", 0L ) )

    // atomically increment sequence value and return the new incremented value
    //
    val dbobject = coll.findAndModify(
      new BasicDBObject( MongoObject.ID.name, seqName.getOrElse( "global" ) ),
      null,
      null,
      false,
      new BasicDBObject( "$inc", new BasicDBObject( "seq", 1L ) ),
      true,
      false
    )

    new MNumericId( Long.unbox( dbobject.get( "seq" ) ) )
  }

  def seqNextValue[A <: MDomain : ru.TypeTag](): MNumericId = seqNextValue( Some( DomainUtils.collectionName( ru.typeOf[A] ) ) )

  /**
   * Obtains a cursor from the given collection. The cursor is prepared to match the given query.
   *
   * @param coll the collection against which to query
   * @param query the query specification
   * @return the matching cursor
   */
  private def prepareDBCursor[A <: MDomain]( coll: DBCollection, query: MongoQuery[A] ): DBCursor = {
    var cursor = coll.find( query.toBSON )

    if ( query.skip != None )
      cursor = cursor.skip( query.skip.get )

    if ( query.limit != None )
      cursor = cursor.limit( query.limit.get )

    if ( query.isOrdered )
      cursor = cursor.sort( query.orderByBSON )

    cursor
  }

  private def checkResult[A <: MDomain]( operation: String, commandResult: CommandResult, affectedDomain: Option[A] = None ): Unit = {
    if ( !commandResult.ok() )
      if ( affectedDomain != None )
        throw new PersistenceException( s"Operation '$operation' affecting domain ${affectedDomain.get.toShortString} failed with error ${commandResult.getErrorMessage}.", commandResult.getException )
      else
        throw new PersistenceException( s"Operation '$operation' failed with error ${commandResult.getErrorMessage}.", commandResult.getException )
  }

  /**
   * Instantiates a `MDomain` class using the TDomainCompanionRegistry provided when this MongoPersistenceProvider
   * was instantiated.
   *
   * @param mo the MongoObject being the base for the TDomain object to be instantiated
   * @tparam A the type of the TDomain class to instantiate (instantiation will only work if a companion object for
   *           the given Domain class exists that extends [[springcampus.core.db.MDomainCompanion]] and offers a method
   *           [[springcampus.core.db.MDomainCompanion.fromMongo]]
   * @return a new instance of the indicated Domain class
   */
  private def instantiateDomain[A <: MDomain]( mo: MongoObject ): A = {
    val dc = domainCompanionRegistry.domainCompanion( mo.discriminator ).asInstanceOf[MDomainCompanion[A]]
    dc.fromMongo( mo, domainCompanionRegistry )
  }
}
