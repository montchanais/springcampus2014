/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.reflect.runtime.{universe => ru}
import scala.collection.mutable.ListBuffer
import com.mongodb.{BasicDBList, BasicDBObject, DBObject}
import org.joda.time.DateTime
import springcampus.core.db.MQCriteria.MQCriterion
import springcampus.core.misc.DateUtils

/**
 * Describes a MongoDB query.
 *
 * @param collection the name of the collection to be queried
 * @param criteria the query criteria
 * @param skip the offset of results to be skipped (for paged queries)
 * @param limit the maximum number of results to return
 * @param _orderBy any order by field pairs, a pair is composed of the field name and either 1 for ascending or -1 for
 *                descending sort order
 */
class MongoQuery[A <: MDomain : ru.TypeTag] private[db] ( val collection: String,
                                                          val criteria: List[MQCriterion],
                                                          val skip: Option[Int],
                                                          val limit: Option[Int],
                                                          private val _orderBy: List[(String,Int)] ) {
  def toBSON: DBObject = {
    val query = new BasicDBObject()
    criteria.foreach( _.addMeToQuery( query ) )
    query
  }

  def isOrdered = !_orderBy.isEmpty

  def orderByBSON: DBObject = {
    val obj = new BasicDBObject()
    _orderBy.foreach( o => obj.append( o._1, o._2 ) )
    obj
  }

  /**
   * Creates a new MQuery instance based on this instance and the additional criterion.
   *
   * @param criterion the criterion to be added to this query
   * @return a new query which corresponds to this instance plus the additional criterion passed as argument
   */
  def addCriterion( criterion: MQCriterion ): MongoQuery[A] =
    new MongoQuery[A]( collection, criterion :: criteria, skip, limit, _orderBy )

  /**
   * Returns the MongoDB JavaScript version of this query.
   */
  override def toString: String = {
    val sb = StringBuilder.newBuilder
    sb.append( s"db.$collection.find(" )
    sb.append( criteria.map( _.toString ).mkString( "{", ",", "}" ) )
    sb.append( ")" )

    if ( isOrdered )
      sb.append( _orderBy.map{ case (attr,dir) => { "\"" + attr + "\":" + dir } }.mkString( ".sort({", ",", "})" ) )

    if ( skip != None )
      sb.append( s".skip(${skip.get})" )

    if ( limit != None )
      sb.append( s".limit(${limit.get})" )

    sb.toString()
  }
}

class MQBuilder[A <: MDomain : ru.TypeTag] private () {

  private val _collection = DomainUtils.collectionName( ru.typeOf[A] )

  private val _criteria = new ListBuffer[MQCriterion]()

  private val _orderBy = new ListBuffer[(String,Int)]()

  private var _skip: Option[Int] = None

  private var _limit: Option[Int] = None

  def add( criterion: MQCriterion ): MQBuilder[A] = {
    _criteria += criterion
    this
  }

  def skip( value: Int ): MQBuilder[A] = {
    _skip = Some( value )
    this
  }

  def limit( value: Int ): MQBuilder[A] = {
    _limit = Some( value )
    this
  }

  def asc( property: String ): MQBuilder[A] = {
    _orderBy += property -> 1
    this
  }

  def desc( property: String ): MQBuilder[A] = {
    _orderBy += property -> -1
    this
  }

  def orderBy( orderBy: Seq[(String,OrderDir.OrderDir)] ): MQBuilder[A] = {
    if ( !_orderBy.isEmpty )
      throw new IllegalStateException( "Method MQBuilder.orderBy cannot be called if some order criteria have already been set before!" )
    orderBy.foreach(
      o => _orderBy += o._1 -> (
        o._2 match {
          case OrderDir.Asc => 1
          case OrderDir.Desc => -1
          case _ => throw new IllegalArgumentException( s"The order direction ${o._2} is not known.")
        }
      )
    )
    this
  }

  def build = new MongoQuery[A]( _collection, _criteria.toList, _skip, _limit, _orderBy.toList )
}

object MQBuilder {
  def apply[A <: MDomain : ru.TypeTag](): MQBuilder[A] = new MQBuilder[A]()
}


object MQCriteria {

  abstract class MQCriterion {
    protected[db] def addMeToQuery( query: DBObject ): DBObject

    protected def addValueToQuery( query: DBObject, property: String, value: AnyRef ): DBObject = {
      query match {
        case arr: BasicDBList => arr.add( new BasicDBObject( property, value ) )
        case obj: BasicDBObject => obj.put( property, value )
        case _ => throw new IllegalArgumentException( s"Unexpected query object class: ${query.getClass.getName}" )
      }
      query
    }

    protected def toBSON( value: Any ): AnyRef = value match {
      case null => null
      case None => null
      case s: Some[_] => toBSON( s.get )
      case id: MId => id.toBSON
      case ref: MRef[_] => ref.refId.toBSON
      case d: MDomain => d.id.toBSON
      case s: String => s
      case d: Double => Double.box( d )
      case i: Int => Int.box( i )
      case l: Long => Long.box( l )
      case b: Boolean => Boolean.box( b )
      case d: DateTime => d.toDate
    }

    protected def toBSONString( value: Any ): String = value match {
      case null => "null"
      case None => "null"
      case s: Some[_] => toBSONString( s.get )
      case id: MId => id.toBSONString
      case ref: MRef[_] => ref.refId.toBSONString
      case d: MDomain => d.id.toBSONString
      case s: String => '\"' + s + '\"'
      case d: Double => d.toString
      case i: Int => i.toString
      case l: Long => l.toString
      case b: Boolean => b.toString
      case d: DateTime => "ISODate(\"" + DateUtils.toString( d ) + "\")"
    }
  }

  def and ( criteria: MQCriterion* ) = new And( criteria )
  class And private[MQCriteria] ( val criteria: Seq[MQCriterion] ) extends MQCriterion {
    protected[db] def addMeToQuery( query: DBObject ): DBObject = {
      val exprArray = new BasicDBList()
      criteria.foreach( _.addMeToQuery( exprArray ) )
      addValueToQuery( query, "$and", exprArray )
    }

    override def toString: String = "\"$and\":" + criteria.map( _.toString ).mkString( "[", ",", "]" )
  }

  def or ( criteria: MQCriterion* ) = new Or( criteria )
  class Or private[MQCriteria] ( val criteria: Seq[MQCriterion] ) extends MQCriterion {
    protected[db] def addMeToQuery( query: DBObject ): DBObject = {
      val exprArray = new BasicDBList()
      criteria.foreach( _.addMeToQuery( exprArray ) )
      addValueToQuery( query, "$or", exprArray )
    }

    override def toString: String = "\"$or\":" + criteria.map( _.toString ).mkString( "[", ",", "]" )
  }

  def refEq ( property: String )( value: Any ) = new Eq( s"$property.${MongoObject.REF_ID.name}", value )
  def eq ( property: String )( value: Any ) = new Eq( property, value )
  class Eq private[MQCriteria] ( val property: String, val value: Any ) extends MQCriterion {
    protected[db] def addMeToQuery( query: DBObject ): DBObject = {
      addValueToQuery( query, property, toBSON( value ) )
    }

    override def toString: String = '\"' + property + "\":" + toBSONString(value)
  }

  def in ( property: String )( values: List[Any] ) = new In( property, values )
  class In private[MQCriteria] ( val property: String, val values: List[Any] ) extends MQCriterion {
    protected[db] def addMeToQuery( query: DBObject ): DBObject = {
      val exprArray = new BasicDBList()
      values.foreach( value => exprArray.add( toBSON( value ) ) )
      addValueToQuery( query, property, new BasicDBObject( "$in", exprArray ) )
    }

    override def toString: String = '\"' + property + "\":{\"$in\":" + values.map( toBSONString( _ ) ).mkString( "[", ",", "]" ) + "}"
  }
}
