/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

/**
 * Signals an abnormal termination of a persistence operation.
 *
 * @param msg an error message
 * @param t the Throwable that caused this exception to be thrown
 */
class PersistenceException( msg: String, t: Throwable ) extends Exception( msg, t ) {
  def this( msg: String ) = this( msg, null )
  def this() = this( null, null )
}

/**
 * Signals an abnormal termination of a persistence operation affecting more than one `Domain` object.
 *
 * @param msg an error message
 * @param t the Throwable that caused this exception to be thrown
 * @param results a map returning for every `Domain` object that should have been object to the failed bulk operation
 *                whether the operation was successful, failed or whether it was skipped for the given object because
 *                the execution is stopped at the first error
 */
class BulkOperationPersistenceException( msg: String, t: Throwable, val results: Map[MDomain,BulkOperationResult.BulkOperationResult] )
  extends PersistenceException( msg, t )

object BulkOperationResult extends Enumeration {
  type BulkOperationResult = Value
  val SUCCEEDED, FAILED, SKIPPED = Value
}
