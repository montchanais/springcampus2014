/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.db

import scala.reflect.runtime.{universe => ru}

trait PersistenceProvider {

  /**
   * Deletes all objects from the underlying database. Use with care and only during development!
   */
  def deleteAll(): Unit

  /**
   * Returns a sequence value.
   *
   * @param seqName the name of the sequence, if not specified a general sequence will be used
   * @return the next value in the sequence
   */
  def seqNextValue( seqName: Option[String] = None ): MNumericId

  /**
   * Returns a sequence value.
   *
   * @tparam A the domain for which to get the next sequence number
   * @return the next value in the sequence
   */
  def seqNextValue[A <: MDomain : ru.TypeTag](): MNumericId

  /**
   * Shuts down the connection to the underlying persistence provider. Should be called prior to terminating
   * a program relying on this PersistenceProvider.
   */
  def shutdown(): Unit

  /**
   * Inserts the given MDomain object. Throws an exception if the object already exists in the DB.
   *
   * @param domain the object to be inserted into the database
   * @tparam A the exact MDomain sub-type of the object to be inserted
   * @return the inserted domain object (the same unchanged object that was inserted into the DB)
   */
  def insert[A <: MDomain]( domain: A ): A

  /**
   * Batch inserts the given MDomain objects. Throws an exception if at least one object already exists in the DB.
   *
   * @param domains the objects to be inserted into the database
   * @tparam A the exact MDomain sub-type of the objects to be inserted
   * @return the inserted domain objects (the same unchanged objects that were inserted into the DB)
   */
  def insert[A <: MDomain]( domains: List[A] ): List[A]

  /**
   * Updates the given MDomain object. Throws an exception if the object does not already exist in the DB.
   *
   * @param domain the object to be updated in the database
   * @tparam A the exact MDomain sub-type of the object to be updated
   * @return the updated domain object (the same unchanged object that was updated in the DB)
   */
  def update[A <: MDomain]( domain: A ): A

  /**
   * Updates the given MDomain objects one by one. Throws an exception if at least one object does not already exist in the DB.
   *
   * @param domains the objects to be updated in the database
   * @tparam A the exact MDomain sub-type of the objects to be updated
   * @return the updated domain objects (the same unchanged objects that were updated in the DB)
   */
  def update[A <: MDomain]( domains: List[A] ): List[A]

  /**
   * Saves the given MDomain object. The object is inserted or updated depending on whether it already exists in the DB or not.
   *
   * @param domain the object to be saved in the database
   * @tparam A the exact MDomain sub-type of the object to be saved
   * @return the saved domain object (the same unchanged object that was saved in the DB)
   */
  def save[A <: MDomain]( domain: A ): A

  /**
   * Saves the given MDomain objects. The objects are inserted or updated depending on whether they already exist in the DB or not.
   *
   * @param domains the objects to be saved in the database
   * @tparam A the exact MDomain sub-type of the objects to be saved
   * @return the saved domain object (the same unchanged object that was saved in the DB)
   */
  def save[A <: MDomain]( domains: List[A] ): List[A]

  /**
   * Retrieves an object from the DB by using its MId.
   *
   * @param id the MId of the requested object
   * @tparam A the expected MDomain sub-type
   * @return an Option either containing the requested object (if it exists in the DB) or None (if it does not exist)
   */
  def findById[A <: MDomain : ru.TypeTag]( id: MId ): Option[A]

  /**
   * Retrieves an object from the DB by using the given reference.
   *
   * @param ref the MRef of the requested object
   * @tparam A the expected MDomain sub-type
   * @return an Option either containing the requested object (if it exists in the DB) or None (if it does not exist)
   */
  def findByRef[A <: MDomain : ru.TypeTag]( ref: MRef[A] ): Option[A] = findById[A]( ref.refId )

  /**
   * Retrieves an object from the DB by using its MId.
   *
   * @param id the MId of the requested object
   * @tparam A the expected MDomain sub-type
   * @return the requested object if it exists in the DB
   * @throws PersistenceException if the requested object does not exist in the DB
   */
  def findByIdOrFail[A <: MDomain : ru.TypeTag]( id: MId ): A

  /**
   * Retrieves an object from the DB by using its reference.
   *
   * @param ref the MRef of the requested object
   * @tparam A the expected MDomain sub-type
   * @return the requested object if it exists in the DB
   * @throws PersistenceException if the requested object does not exist in the DB
   */
  def findByRefOrFail[A <: MDomain : ru.TypeTag]( ref: MRef[A] ): A = findByIdOrFail[A]( ref.refId )

  /**
   * Retrieves all objects from the DB where the IDs match the given ones.
   *
   * @param ids the IDs of the requested objects
   * @param orderBy list of the fields to order by, each list element is a pair consisting of the property to order by
   *                and the order direction
   * @tparam A the expected MDomain sub-type
   * @return the requested objects or an empty List if no object is found
   */
  def findByIds[A <: MDomain : ru.TypeTag]( ids: List[MId], orderBy: (String,OrderDir.OrderDir)* ): List[A]

  /**
   * Retrieves all objects from the DB where the IDs match the given reference IDs.
   *
   * @param refs the references to the requested objects
   * @param orderBy list of the fields to order by, each list element is a pair consisting of the property to order by
   *                and the order direction
   * @tparam A the expected MDomain sub-type
   * @return the requested objects or an empty List if no object is found
   */
  def findByRefs[A <: MDomain : ru.TypeTag]( refs: List[MRef[A]], orderBy: (String,OrderDir.OrderDir)* ): List[A] =
    findByIds[A]( refs.map( _.refId ), orderBy : _* )

  /**
   * Returns all entities of the given collection where an MRef attribute's refId value corresponds to
   * the given MId (in SQL terms: retrieves elements by foreign key).
   *
   * @param refName the name of the reference attribute
   * @param refId the MId by which to search
   * @param orderBy list of the fields to order by, each list element is a pair consisting of the property to order by
   *                and the order direction
   * @tparam A the expected MDomain sub-type
   * @return all objects referencing the given MId or an empty List if no object is found
   */
  def findByRefAttr[A <: MDomain : ru.TypeTag]( refName: String, refId: MId, orderBy: (String,OrderDir.OrderDir)* ): List[A]

  /**
   * Returns all entities of the given collection where an MRef attribute's refId value corresponds to
   * one of the given MIds (in SQL terms: retrieves elements by foreign key).
   *
   * @param refName the name of the reference attribute
   * @param refIds the list of MIds to search for
   * @param orderBy list of the fields to order by, each list element is a pair consisting of the property to order by
   *                and the order direction
   * @tparam A the expected MDomain sub-type
   * @return all objects referencing one of the given MIds or an empty List if no object is found
   */
  def findByRefAttr[A <: MDomain : ru.TypeTag]( refName: String, refIds: List[MId], orderBy: (String,OrderDir.OrderDir)* ): List[A]

  /**
   * Returns all elements of the given collection.
   *
   * @tparam A the type of the retrieved objects
   * @return a List containing all objects of the given collection (can be empty if the collection is empty)
   */
  def findAll[A <: MDomain : ru.TypeTag](): List[A]

  /**
   * Finds all elements that match the given query object.
   *
   * @param query the query to be executed
   * @tparam A the type of the retrieved objects
   * @return a List containing all objects that match the query (can be empty if no object matches)
   */
  def findByQuery[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): List[A]

  /**
   * Finds a single element matching the given query.
   *
   * @param query the query to be executed
   * @tparam A the type of the retrieved objects
   * @return an Option containing the matching object
   */
  def findOneByQuery[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): Option[A]

  /**
   * Finds a single element matching the given query.
   *
   * @param query the query to be executed
   * @tparam A the type of the retrieved objects
   * @return the matching object
   * @throws PersistenceException if there is no object that matches the query
   */
  def findOneByQueryOrFail[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): A

  /**
   * Counts all objects that are part of the database collection for the given domain.
   *
   * @tparam A the type of domain for which to determine the number of all existing objects in the DB
   * @return the number of all existing objects in the DB matching the domain class
   */
  def countAll[A <: MDomain : ru.TypeTag](): Long

  /**
   * Counts the number of objects that would be returned by the given query.
   *
   * @param query the query to be executed
   * @tparam A the type of domain for which to execute the query
   * @return the number of objects matching the given query
   */
  def count[A <: MDomain : ru.TypeTag]( query: MongoQuery[A] ): Long
}

object OrderDir extends Enumeration {
  type OrderDir = Value
  val Asc, Desc = Value
}
