/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.domain

import springcampus.core.db._

case class Club( id: MNameId,
                 seeding: Int,
                 name: String,
                 country: MRef[Country],
                 place: String,
                 arena: String,
                 latitude: Double,
                 longitude: Double ) extends MDomain {

  override lazy val attributes =
    Map(
      'seeding -> seeding,
      'name -> name,
      'country -> country,
      'place -> place,
      'arena -> arena,
      'latitude -> latitude,
      'longitude -> longitude
    )

  override lazy val attrsUiMiniVersion =
    Map(
      'id -> id,
      'name -> name,
      'country -> country
    )

  def ref = new MRef[Club]( id )
}

object Club extends MDomainCompanion[Club]{
  def createId( id: String ) = new MNameId( id )

  def fromMongo( src: MongoObject, dcr: MDomainCompanionRegistry ): Club =
    Club(
      src.id.asNameId,
      src.attrInt( 'seeding ),
      src.attrString( 'name ),
      src.attrRef[Country]( 'country, dcr ),
      src.attrString( 'place ),
      src.attrString( 'arena ),
      src.attrDouble( 'latitude ),
      src.attrDouble( 'longitude )
    )
}
