/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.domain

import springcampus.core.db._

case class Country ( id: MNameId,
                     name: String ) extends MDomain {

  override lazy val attributes =
    Map(
      'name -> name
    )

  def ref = new MRef[Country]( id )
}

object Country extends MDomainCompanion[Country]{
  def createId( id: String ) = new MNameId( id )

  def fromMongo(src: MongoObject, dcr: MDomainCompanionRegistry ): Country =
    Country(
      src.id.asNameId,
      src.attrString( 'name )
    )
}
