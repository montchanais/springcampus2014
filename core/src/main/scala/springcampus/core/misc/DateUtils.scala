/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.misc

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, ISODateTimeFormat}

object DateUtils {

  private final lazy val DISPLAY_DATE_TIME_FORMATTER = DateTimeFormat.forPattern( "EEE dd MMM yyyy - HH:mm" )

  /**
   * Returns the next date having the given dayOfWeek and following the given date.
   *
   * @param d the date for which to return the next day having the given dayOfWeek
   * @param dayOfWeek use a [[org.joda.time.DateTimeConstants]] constant to define the next dayOfWeek to return
   * @return the next date following the given one having the given dayOfWeek (if the given date is Saturday and
   *         a Saturday is requested, this method returns the Saturday in one week)
   */
  def getNextWeekday( d: DateTime, dayOfWeek: Int ): DateTime = {
    if ( d.getDayOfWeek < dayOfWeek )
      d.withDayOfWeek( dayOfWeek )
    else
      d.plusWeeks( 1 ).withDayOfWeek( dayOfWeek )
  }

  /**
   * Parses a DateTime string which is supposed to combine a full date and time, separated by a 'T'
   * (yyyy-MM-dd'T'HH:mm:ss.SSSZZ). The time zone offset is 'Z' for zero, and of the form '±HH:mm' for non-zero.
   *
   * @param s the String to be parsed
   * @return the corresponding DateTime instance
   * @see [[org.joda.time.format.ISODateTimeFormat.dateTimeParser()]]
   */
  def fromString( s: String ): DateTime = {
    ISODateTimeFormat.dateTimeParser().parseDateTime( s )
  }

  /**
   * Converts a DateTime instance into a String showing a full date and time, separated by a 'T'
   * (yyyy-MM-dd'T'HH:mm:ss.SSSZZ). The time zone offset is 'Z' for zero, and of the form '\u00b1HH:mm' for non-zero.
   *
   * @param d the DateTime to be converted to a String
   * @return a String in the form yyyy-MM-dd'T'HH:mm:ss.SSSZZ
   * @see [[org.joda.time.format.ISODateTimeFormat.dateTime()]]
   */
  def toString( d: DateTime ): String = {
    d.toString( ISODateTimeFormat.dateTime() )
  }

  /**
   * Converts the given DateTime instance to a human readable String representation which is optimized for
   * screen display. The format of the returned String is `` (see [[org.joda.time.format.DateTimeFormat]] for details).

   * @param d the DateTime instance to be beautified
   * @return the generated String
   */
  def toDisplayDateTimeString( d: DateTime ): String = {
    d.toString( DISPLAY_DATE_TIME_FORMATTER )
  }

  /**
   * Converts the given optional DateTime instance to a human readable String representation which is optimized for
   * screen display. The format of the returned String is `` (see [[org.joda.time.format.DateTimeFormat]] for details).

   * @param d the optional DateTime instance to be beautified
   * @return the generated String or "n/a" if the given DateTime instance was None
   */
  def toDisplayDateTimeString( d: Option[DateTime] ): String = {
    if ( d == None )
      "n/a"
    else
      d.get.toString( DISPLAY_DATE_TIME_FORMATTER )
  }
}
