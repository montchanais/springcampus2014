/*
 * Copyright (c) 2013 Virtual Hockey League Project (https://github.com/montblanc/vhl)
 * All rights reserved.
 *
 * This file is part of VHLcs (Virtual Hockey League Control Software).
 *
 * VHLcs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VHLcs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VHLcs.  If not, see <http://www.gnu.org/licenses/>.
 */
package springcampus.core.misc

import scala.language.reflectiveCalls

/**
 * Groups together general multi-purpose utility methods.
 */
object GeneralUtils {

  /**
   * Represents the result of a timed operation.
   *
   * @param result the timed operation's result
   * @param duration the operation's duration (if timing is switched on - if not 0 is returned)
   * @tparam A the type of the operation's result
   */
  case class TimedOperationResult[A]( result: A, duration: Long )

  /**
   * Executes the given operation and times it if the timer is enabled.
   *
   * @param enableTimer indicates whether the timer should be enabled or not
   * @param op the operation to be timed while being performed
   * @tparam A the type of the operation's result
   * @return the operation's actual computation result and the time it took to execute the operation (if the timer
   *         is disabled by setting the argument `enableTimer` to `false`, `0` is returned as duration)
   */
  def timedOperation[A]( enableTimer: Boolean )( op: => A ): TimedOperationResult[A] = {
    val startTime: Long = {
      if ( enableTimer )
        System.currentTimeMillis();
      else
        0L
    }

    TimedOperationResult( op, if ( enableTimer ) System.currentTimeMillis() - startTime else 0L )
  }

  /**
   * Executes the given operation and times it.
   *
   * @param op the operation to be timed while being performed
   * @tparam A the type of the operation's result
   * @return the operation's actual computation result and the time it took to execute the operation
   */
  def timedOperation[A]( op: => A ): TimedOperationResult[A] = timedOperation( true )( op )

  /**
   * Executes the given operation relying on a closeable resource and closes the resource after the computation.
   * 
   * @param resource the closeable resource that should be closed after the computation
   * @param op the operation requiring the closeable resource
   * @tparam A the type of the resource
   * @tparam B the type of the operation's result
   * @return the operation's result
   */
  def withResource[A <: { def close(): Unit }, B]( resource: A )( op: => B ): B = {
    try {
      op
    }
    finally {
      resource.close()
    }
  }
}
