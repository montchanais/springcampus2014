/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package springcampus.core.service

import com.typesafe.scalalogging.log4j.Logging
import springcampus.core.db.{MId, MRef, PersistenceProvider}
import springcampus.core.domain.{Club, Country}

trait InitializationService extends Logging {
  val persistenceProvider: PersistenceProvider

  /**
   * Any code that must always be run at any application start-up should go into this method.
   */
  def bootstrap() {
    // At this early stage of development, let us just reset the database every time
    resetDatabase()
  }

  /**
   * Resets the game database to its delivery state, i.e. the database is deleted and then filled with
   * example data suitable to be used for testing.
   */
  def resetDatabase() {

    // start with a clean environment
    //
    persistenceProvider.deleteAll()

    // create countries
    //
    val countries =
      List[Country](
        Country( MId.name( "CH" ), "Switzerland" ),
        Country( MId.name( "DE" ), "Germany" ),
        Country( MId.name( "FR" ), "France" )
      )

    // create some clubs
    //
    val clubs =
      List[Club](
        Club( MId.name( "CH-BER" ), 1, "SC Bern", MRef.fromName[Country]( "CH" ), "Bern", "PostFinance-Arena", 46.95864, 7.46866 ),
        Club( MId.name( "CH-BIE" ), 2, "EHC Biel", MRef.fromName[Country]( "CH" ), "Biel", "Eisstadion Biel", 47.15295, 7.27522 ),
        Club( MId.name( "CH-DAV" ), 3, "HC Davos", MRef.fromName[Country]( "CH" ), "Davos", "Vaillant Arena", 46.79856, 9.8264 ),
        Club( MId.name( "CH-FRI" ), 4, "HC Fribourg-Gottéron", MRef.fromName[Country]( "CH" ), "Fribourg", "Patinoire Saint Léonhard", 46.81736, 7.15546 ),

        Club( MId.name( "DE-KOL" ), 1, "Kölner Haie", MRef.fromName[Country]( "DE" ), "Köln", "Kölnarena", 50.93847, 6.9829 ),
        Club( MId.name( "DE-MUN" ), 2, "EHC Red Bull München", MRef.fromName[Country]( "DE" ), "München", "Olympia-Eisstadion", 48.17537, 11.55703 ),
        Club( MId.name( "DE-EBB" ), 3, "Eisbären Berlin", MRef.fromName[Country]( "DE" ), "Berlin", "o2 World", 52.50627, 13.4436 ),
        Club( MId.name( "DE-MAN" ), 4, "Adler Mannheim", MRef.fromName[Country]( "DE" ), "Mannheim", "SAP Arena", 49.4644, 8.5184 ),

        Club( MId.name( "FR-GAP" ), 1, "Rapaces de Gap", MRef.fromName[Country]( "FR" ), "Gap", "L'Alp Arena", 44.56042, 6.08569 ),
        Club( MId.name( "FR-GRE" ), 2, "Brûleurs de Loups de Grenoble", MRef.fromName[Country]( "FR" ), "Grenoble", "Patinoire Pôle Sud", 45.15779, 5.73454 ),
        Club( MId.name( "FR-MOR" ), 3, "HC MAG Pengouins", MRef.fromName[Country]( "FR" ), "Morzine", "Palais des Sports", 46.18401, 6.70518 ),
        Club( MId.name( "FR-ROU" ), 4, "Dragons de Rouen", MRef.fromName[Country]( "FR" ), "Rouen", "Patinoire de l'Ile Lacroix", 49.43079, 1.10353 )
      )

    // persist everything
    //
    persistenceProvider.insert( countries )
    persistenceProvider.insert( clubs )
  }
}
