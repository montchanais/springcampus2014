resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

// Add support for the Play Framework
addSbtPlugin( "com.typesafe.play" % "sbt-plugin" % "2.2.2" )

// Add support for IntelliJ IDEA
addSbtPlugin( "com.github.mpeltonen" % "sbt-idea" % "1.6.0" )

