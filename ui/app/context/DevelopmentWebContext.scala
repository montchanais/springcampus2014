/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package context

import controllers.{EditClubController, MainController}
import springcampus.core.context.DevelopmentContext
import springcampus.core.domain.{Country, Club}
import springcampus.core.db.{MRef, PersistenceProvider}
import utils.{JsonConverter, JsonMRefCustomConverter}

class DevelopmentWebContext private[context]() extends DevelopmentContext with WebContext {

  lazy val jsonConverter: JsonConverter = new JsonConverter {
    val persistenceProvider: PersistenceProvider = DevelopmentWebContext.this.persistenceProvider
    val mrefCustomConverter: JsonMRefCustomConverter = new JsonMRefCustomConverter {
      def apply(ref: MRef[_], persistenceProvider: PersistenceProvider ): Map[Symbol, Any] = ref.collection match {
        case "Country" => persistenceProvider.findByIdOrFail[Country]( ref.refId ).attrsUiMiniVersion
        case "Club" => persistenceProvider.findByIdOrFail[Club]( ref.refId ).attrsUiMiniVersion
        case _ => Map()
      }
    }
  }

  lazy val mainController: MainController = new MainController( persistenceProvider, jsonConverter )

  lazy val editClubController: EditClubController = new EditClubController( persistenceProvider, jsonConverter )
}
