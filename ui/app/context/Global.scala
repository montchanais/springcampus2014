/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package context

import play.api.{Application, GlobalSettings}
import play.Logger
import springcampus.core.context.{EnvironmentType, Context}
import controllers.{EditClubController, MainController}

object Global extends GlobalSettings {

  /**
   * Returns the current environment.
   */
  val currentEnvironmentType = EnvironmentType.Dev  // hardcoded to DEV - at a later stage this needs to be dynamic

  /**
   * Returns the current DI (dependency injection) context.
   */
  lazy val context: WebContext =
    currentEnvironmentType match {
      case EnvironmentType.Dev => new DevelopmentWebContext()
      case _ => throw new IllegalStateException( s"Environment type '${currentEnvironmentType}' is not supported." )
    }

  override def onStart(app: Application ){
    Logger.info( "Starting SpringCampus web application..." )
    context.initializationService.bootstrap()
  }

  override def onStop(app: Application ){
    Logger.info( "Shutting down SpringCampus web application..." )
    context.persistenceProvider.shutdown()
  }

  override def getControllerInstance[A]( controllerClass: Class[A] ): A = {
    if ( controllerClass == classOf[MainController] )
      context.mainController.asInstanceOf[A]
    else if ( controllerClass == classOf[EditClubController])
      context.editClubController.asInstanceOf[A]
    else
      super.getControllerInstance( controllerClass )
  }
}
