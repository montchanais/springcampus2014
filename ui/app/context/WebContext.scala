/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package context

import springcampus.core.context.Context
import controllers.{EditClubController, MainController}
import utils.JsonConverter

/**
 * Web application specific extension of the VHL DI context.
 */
trait WebContext extends Context {
  val jsonConverter: JsonConverter
  val mainController: MainController
  val editClubController: EditClubController
}
