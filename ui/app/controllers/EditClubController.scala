/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package controllers

import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import springcampus.core.db.{MRef, MId, PersistenceProvider}
import springcampus.core.domain.{Country, Club}
import utils.JsonConverter
import play.api.data.format.Formats
import views.html.helper.FieldConstructor

case class ClubFormData( id: String,
                         name: String,
                         country: String,
                         place: String,
                         arena: String,
                         latitude: Double,
                         longitude: Double )

object ClubFormData {
  def fromClub( club: Club ): ClubFormData =
    ClubFormData( club.id.toString, club.name, club.country.refId.toString, club.place, club.arena, club.latitude, club.longitude )
}

object ClubFormHelpers {
  implicit val clubFormFields = FieldConstructor( views.html.inputhelper.f )
}

class EditClubController( private val persistenceProvider: PersistenceProvider,
                          private val jsonConverter: JsonConverter ) extends Controller {

  val countries = persistenceProvider.findAll[Country]().sortBy( _.name )

  val clubForm: Form[ClubFormData] = Form(
    mapping(
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,
      "country" -> nonEmptyText( minLength = 2, maxLength = 2 ),
      "place" -> nonEmptyText,
      "arena" -> nonEmptyText,
      "latitude" -> of(Formats.doubleFormat),
      "longitude" -> of(Formats.doubleFormat)
    )(ClubFormData.apply)(ClubFormData.unapply)
  )

  /**
   * Display a Club edit form for the club with the given ID.
   */
  def editClub( clubId: String ) = Action {
    val optClub = persistenceProvider.findById[Club]( MId.name( clubId ) )

    optClub match {
      case None => BadRequest( s"No Club with Id $clubId exists." )
      case Some( club ) => {
        Ok( views.html.edit_club( countries, clubForm.fill( ClubFormData.fromClub( club ) ) ) )
      }
    }
  }

  def clubEdited() = Action { implicit request =>
    val receivedForm = clubForm.bindFromRequest
    receivedForm.fold(
      formWithErrors => {
        BadRequest( views.html.edit_club( countries, formWithErrors ) )
      },
      clubData => {
        // get ID and check that an existing club is edited
        //
        persistenceProvider.findById[Club]( MId.name( clubData.id ) ) match {
          case None => {
            BadRequest( views.html.edit_club( countries, receivedForm.withGlobalError( s"Tried to edit non-existing club with id ${clubData.id}" ) ) )
          }
          case Some( club ) => {
            val modifiedClub = club.copy( name = clubData.name, country = MRef.fromName[Country]( clubData.country ),
              place = clubData.place, arena = clubData.arena, latitude = clubData.latitude, longitude = clubData.longitude )
            persistenceProvider.save( modifiedClub )
            Redirect( routes.MainController.index() )
          }
        }
      }
    )
  }
}
