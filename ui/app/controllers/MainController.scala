/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package controllers

import play.api.mvc._
import springcampus.core.db.{MId, PersistenceProvider}
import springcampus.core.domain.{Club, Country}
import utils.JsonConverter

class MainController( private val persistenceProvider: PersistenceProvider,
                      private val jsonConverter: JsonConverter ) extends Controller {



  def index = Action {
    val countries = persistenceProvider.findAll[Country]().sortBy( _.name )
    Ok( views.html.index( countries ) )
  }

  def country( countryId: String ) = Action {
    val country = persistenceProvider.findById[Country]( MId.name( countryId ) )

    if ( country != None )
      Ok( jsonConverter.toJsObject( country.get ) )
    else
      BadRequest( s"No Country with Id $countryId exists." )
  }

  def clubsByCountry( countryId: String ) = Action {
    val country = persistenceProvider.findById[Country]( MId.name( countryId ) )

    if ( country == None )
      BadRequest( s"No Country with Id $countryId exists." )
    else {
      val clubs = persistenceProvider.findByRefAttr[Club]( "country", country.get.id )
      Ok( jsonConverter.toJsArray( clubs ) )
    }
  }
}
