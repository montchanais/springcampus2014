/*
 * Copyright (c) 2014 Jan Janke (CERN)
 * All rights reserved.
 *
 * This file is part of the "Scala & Play Hands-On" presentation given by Jan Janke
 * at the CERN Spring Campus in April 2014 in Gijón (Spain).
 */

package utils

import play.api.libs.json.{JsNull, JsValue}
import org.joda.time.DateTime
import springcampus.core.db._
import springcampus.core.misc.DateUtils
import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import play.api.libs.json.JsBoolean
import play.api.libs.json.JsNumber
import play.api.libs.json.JsObject

trait JsonConverter {

  val persistenceProvider: PersistenceProvider

  val mrefCustomConverter: JsonMRefCustomConverter

  /**
   * Converts the given List of instances into a JsArray.
   *
   * @param domains the List of domain instances to be converted
   * @tparam A the type of the domain object that is converted
   * @return the generated JsArray
   */
  def toJsArray[A <: { def attributes: Map[Symbol,Any] }]( domains: List[A] ): JsArray =
    JsArray( domains.map( toJsObject( _ ) ) )

  /**
   * Converts the given instance into a JsObject.
   *
   * @param domain the domain to be converted
   * @tparam A the type of the domain object that is converted
   * @return the generated JsObject
   */
  def toJsObject[A <: { def attributes: Map[Symbol,Any] }]( domain: A ): JsObject = {

    // if the object to be converted is of type MDomain, convert the object first to an MEmbeddedDomain
    //
    val attributes: Map[Symbol,Any] =
      domain match {
        case d: MDomain => d.attrsUiFullVersion
        case _ => domain.attributes
      }

    toJsObjectInternal( attributes, 1 )
  }

  private def toJsObjectInternal( attributes: Map[Symbol,Any],
                                  depth: Int ): JsObject = {
    JsObject(
      attributes.map{
        case( key, value ) => key.name -> toJsValue( value, depth )
      }.toSeq
    )
  }

  private def transformMapKey( key: Any ): String = key match {
    case s: Symbol => s.name
    case s: String => s
    case r: MRef[_] => r.refId.toString
    case _ => throw new IllegalArgumentException( s"Map key type ${key.getClass.getName} is not supported." )
  }

  private def toJsValue( value: Any,
                         depth: Int ): JsValue = value match {
    case null => JsNull
    case None => JsNull
    case Some( optVal ) => toJsValue( optVal, depth )
    case s: String => JsString( s )
    case i: Int => JsNumber( i )
    case l: Long => JsNumber( l )
    case f: Float => JsNumber( f )
    case d: Double => JsNumber( d )
    case b: Boolean => JsBoolean( b )
    case d: DateTime => JsString( DateUtils.toString( d ) )
    case n: MNumericId => JsNumber( n.value )
    case i: MId => JsString( i.value.toString )
    case r: MRef[_] => {
      val attrs = mrefCustomConverter( r, persistenceProvider )
      if ( attrs.isEmpty )
        toJsValue( r.refId, depth )
      else
        toJsObjectInternal( attrs, depth + 1 )
    }
    case d: MDomain => toJsObjectInternal( d.attrsUiFullVersion, depth + 1 )
    case e: MEmbeddedDomain => JsObject( e.attrsUiFullVersion.map{ case( key, value ) => key.name -> toJsValue( value, depth + 1 ) }.toSeq )
    case l: List[_] => JsArray( l.map( toJsValue( _, depth ) ) )
    case m: Map[_,_] => JsObject( m.map{ case ( key, value ) => transformMapKey( key ) -> toJsValue( value, depth + 1 ) }.toSeq )
    case _ => throw new IllegalArgumentException( s"The class ${value.getClass.getName} of value $value is not supported by JSON transformation." )
  }
}

/**
 * Defines a custom attribute value converter for Domain instances. This attribute converter can be defined to take
 * over the conversion of the attribute value into a Json value (JsValue) whenever an attribute with a certain name
 * is encountered during the Domain/Json conversion process.
 */
trait JsonAttrCustomConverter {
  def apply( attrName: String, attrValue: Any, persistenceProvider: PersistenceProvider ): JsValue
}

/**
 * Defines a custom MRef attribute value converter for Domain instances. This attribute converter can be defined to take
 * over the conversion of MRef attribute values into a Json value (JsValue) whenever an MRef attribute for a certain
 * Mongo collection is encountered during the Domain/Json conversion process. Such a custom converter can for instance
 * be used to replace the ID of a referenced entity with the actual object that is referenced.
 */
trait JsonMRefCustomConverter {
  def apply( ref: MRef[_], persistenceProvider: PersistenceProvider ): Map[Symbol,Any]
}
