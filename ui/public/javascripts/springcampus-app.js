'use strict';

var springcampusApp = angular.module('springcampusApp', [
  'springcampusControllers',
  'springcampusServices',
  'springcampusDirectives',
  'ngRoute'
]);

springcampusApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: '/assets/partials/home.html',
        controller: 'HomeCtrl'
      }).
      when('/country_clubs/:countryId', {
        templateUrl: '/assets/partials/country_clubs.html',
        controller: 'ClubListCtrl'
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]
);
