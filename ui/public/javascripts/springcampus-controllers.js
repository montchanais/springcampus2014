'use strict';

var vhlControllers = angular.module('springcampusControllers', []);

vhlControllers.controller('HomeCtrl', ['$scope',
  function($scope) {
    // TODO
  }]
);

vhlControllers.controller('ClubListCtrl', ['$scope','$routeParams','springcampusDataService',
    function($scope, $routeParams, springcampusDataService) {
      $scope.country = springcampusDataService.getCountry($routeParams.countryId);
      $scope.clubs = springcampusDataService.getClubsByCountry($routeParams.countryId, function( clubs ){
        // order the clubs array by club name and split the array in two halves as we may display the clubs in two columns
        clubs.sort(function(c1,c2){return c1.name > c2.name});
        $scope.clubsHalf1 = clubs.slice( 0, clubs.length / 2 );
        $scope.clubsHalf2 = clubs.slice( clubs.length / 2, clubs.length );
      });
      $scope.countryFlagClass = function( flagSize, countryId ){ return springcampusDataService.getCountryFlagCSSClass( flagSize, countryId ) };
    }]
);
