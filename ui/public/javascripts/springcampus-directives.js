'use strict';

var springcampusDirectives = angular.module('springcampusDirectives', []);

springcampusDirectives.directive('springcampusClubInfo', function () {
  var objDirectiveDef = {
    restrict: 'E',
    templateUrl: '/assets/directives/club_info.html',
    scope: {
      club: '='
    },
    link: function (scope, elem, attrs) {
      scope.$watch( 'club',
        function(club, oldValue) {
          if ( club == undefined )
            return;

          // set the right class for the club's country flag
          var divPlace = $( '#club-info-place-' + club.id );
          divPlace.addClass( 'flag-24-' + club.country.id );
        }
      )
    }
  }

  return objDirectiveDef;
});

springcampusDirectives.directive('springcampusCountryHeader', function () {
  var objDirectiveDef = {
    restrict: 'E',
    templateUrl: '/assets/directives/country_header.html',
    scope: {
      country: '=',
      activeTab: '@'
    },
    link: function (scope, elem, attrs) {
      // mark the active tab and remove its link
      var activeTab = $( '#springcampus-tab-' + scope.activeTab );
      activeTab.addClass( 'active' );
      activeTab.find( 'a' ).attr( 'href', '' );
    }
  }

  return objDirectiveDef;
});
