'use strict';

var vhlServices = angular.module('springcampusServices', ['ngResource']);

vhlServices.factory('springcampusDataService', ['$resource',
  function($resource){
    var objCountries = new Object();
    var objClubsByCountry = new Object();

    return {
      getCountryFlagCSSClass: function( flagSize, countryId ){
        return "flag-" + flagSize + " flag-" + flagSize + "-" + countryId;
      },

      getCountry: function( countryId, fnCallbackSuccess, fnCallbackError ){
        if ( !objCountries.hasOwnProperty( countryId ) )
            objCountries[countryId] = $resource('country/:countryId').get( {countryId: countryId}, fnCallbackSuccess, fnCallbackError );
        else if ( typeof fnCallbackSuccess === 'function' )
          fnCallbackSuccess( objCountries[countryId] );

        return objCountries[countryId];
      },

      getClubsByCountry: function( countryId, fnCallbackSuccess, fnCallbackError ){
        if ( !objClubsByCountry.hasOwnProperty( countryId ) )
            objClubsByCountry[countryId] = $resource('clubsByCountry/:countryId').query( {countryId: countryId}, fnCallbackSuccess, fnCallbackError );
        else if ( typeof fnCallbackSuccess === 'function' )
          fnCallbackSuccess( objClubsByCountry[countryId] );

        return objClubsByCountry[countryId];
      }
    }
  }]
);
